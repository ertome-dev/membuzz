/*
 * Third party
 */
//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/bootstrap/dist/js/bootstrap.js


function myFunction() {
	var x = document.getElementById('myTopnav');
	if (x.className === 'topnav'){
		x.className += ' responsive';
	} else {
		x.className = 'topnav';
	}
}
function showElement(elNumber) {

	var elements = document.querySelector('.menu-container').children;
	for (var i = 0, length = elements.length; i < length; i++) {
		if (i === elNumber) {
			elements[i].classList.add('visible');
		} else {
			elements[i].classList.remove('visible');
		}
	}
}

function showMenu() {
	var menuBackground = document.getElementById('menu-container'),
		menuContainer = document.getElementById('menu-left-side');

	if (menuBackground.classList.contains('visible-background')) {
		menuBackground.classList.remove('visible-background');
	} else {
		menuBackground.classList.add('visible-background');
	}

	if (menuContainer.classList.contains('visible')) {
		menuContainer.classList.remove('visible');
	} else {
		menuContainer.classList.add('visible');
	}

}

function showTab(tab) {
	var tabs = document.querySelectorAll('.tab'),
		sections = document.querySelectorAll('.tab-section'),
		activeTab = document.getElementById(tab + '-tab'),
		activeSection = document.getElementById(tab + '-section');

	for (var i = 0, len = tabs.length; i < len; i++ ) {
		if (tabs[i].classList.contains('active')) {
			tabs[i].classList.remove('active');
		}
		if (sections[i].classList.contains('active')) {
			sections[i].classList.remove('active');
		}


	}

	activeTab.classList.add('active');
	activeSection.classList.add('active');

}



















